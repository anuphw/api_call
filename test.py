import pandas as pd
import requests
import PySimpleGUI as sg
from pathlib import Path
import time
import os
import json
import socket
from datetime import datetime
import re

def is_connected():
    try:
        socket.create_connection(("1.1.1.1", 53))
        return True
    except OSError:
        pass
    return False

def append_df_to_excel(filename, df, sheet_name='Sheet1',index=False):
    from openpyxl import load_workbook
    import pandas as pd
    writer = pd.ExcelWriter(filename, engine='openpyxl')
    try:
        writer.book = load_workbook(filename)
    except:
        pass
    df.to_excel(writer, sheet_name, index=index)
    writer.save()

def is_numeric(x):
    try:
        if float(x) == float(x):
            return True
        else:
            return False
    except:
        return False

URL = "https://thetexbazaar.com/api"
ENTRY_SCRIPT = "data_entry_actions.php"
WATCHLIST_SCRIPT = "yarn_watchlist.php"
KEY = "djfkjhflsdhfo87w8uijfdkljsd"
ACTION = "insert"
HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

def f():
    exported = kill = False
    sg.theme('Dark Brown')  # please make your creations colorful
    if is_connected():
        initial_status = 'Choose a file'
    else:
        initial_status = 'Check your internet connection'
    layout = [[sg.Text('Filename',key='_fname_')],
              [sg.Input(key='_input_'), sg.FileBrowse(key='_browser_')],
              [sg.Text(initial_status, justification='l', size=(30,1), font='Mambo 10' ,key='-stats-')],
              [sg.B('Import'),sg.B('Export'), sg.Cancel('Cancel')]]
    window = sg.Window('Get filename example', layout)
    while True:
        event, values = window.read()
        if event in (sg.WIN_CLOSED, '_EXIT_', 'Cancel'):
            kill = True
            break
        elif event == 'Import':
            if values['_input_'] == "":
                sg.popup('Please enter the correct filename')
                continue
            dir = str(Path(values['_input_']).parent.absolute())
            filestem = Path(values['_input_']).stem
            if not os.path.isdir(dir):
                sg.popup('Please enter the correct filename')
                continue
            req = requests.get(f"{URL}/{WATCHLIST_SCRIPT}?key={KEY}",headers = HEADERS)
            data = pd.DataFrame.from_records(json.loads(req.content.decode('utf-8'))['data'])
            # print(data)
            window['-stats-'].update(f"Importing data into {values['_input_']}")
            cols = ['product_id', 'Count', 'Product Type', 'Company', 'Packing KG', 'Cone',
                    'Price Type', 'Price', 'Change', 'Sign', 'Last updated', 'Change(%)',datetime.now().strftime('%Y-%m-%d')]
            data['Price'] = ""
            data[datetime.now().strftime('%Y-%m-%d')] = 'insert'
            data[cols].to_excel(f'{dir}/{filestem}.xlsx',sheet_name = 'Data', index=False)
            # append_df_to_excel(f'{dir}/{filestem}.xlsx',data[cols],sheet_name=datetime.now().strftime('%Y-%m-%d'),index=False)
            break
        elif event == 'Export':
            if values['_input_'] == "":
                sg.popup('Please enter the correct filename')
                continue
            dir = str(Path(values['_input_']).parent.absolute())
            filestem = Path(values['_input_']).stem
            if not os.path.isdir(dir):
                sg.popup('Please enter the correct filename')
                continue
            d = pd.read_excel(values['_input_'],sheet_name='Data')
            dt_col = None
            for c in d.columns:
                if re.match('[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]',c):
                    dt_col = c
                    break
            if not dt_col:
                sg.popup('No date column found in the excel file')
                continue
            print(f'date column is {dt_col}')
            d['status'] = ""
            num_rows = len(d)
            i = 0
            while True:
                _, _ = window.read(timeout=1000)
                id = d.product_id.loc[i]
                price = d.Price.loc[i]
                action = d.loc[i,dt_col]
                window['-stats-'].update(f'Status: exporting {i+1}/{num_rows}')
                if is_numeric(price):
                    req = requests.get(f'{URL}/{ENTRY_SCRIPT}?key={KEY}&product_id={id}&price={price}&date={dt_col}&action={action}',headers = HEADERS)
                    status = req.status_code
                else:
                    status = 'Not a number'
                    print(status)
                d.loc[i,'status'] = status
                print(i, action, price, status)
                i += 1
                if i == num_rows:
                    break
            exported = True
            break
    i=0
    if kill:
        window.close()
    else:
        while True:
            event, values = window.read(timeout=1000)
            window['-stats-'].update(f'Done...closing in {3-i} s')
            i += 1
            if i == 3:
                break
        window.close()
        if exported:
            fname = 'upload_results_' + datetime.now().strftime('%Y-%m-%d') + '.csv'
            x = d.to_csv(dir + '/' + fname,index=False)

if __name__ == '__main__':
    f()

